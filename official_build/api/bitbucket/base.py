'''
Bitbucket API
'''
import re
from typing import Optional

import requests
import yaml


class BitbucketApiServiceError(Exception):
    pass


class BitbucketApiService:

    def get_latest_tag(self, repository_path: str) -> Optional[str]:
        """
        Gets the latest tag of a repository (sort alphabetically)
        :param repository_path: Repository path (ex: account/repo)
        :return: Latest tag
        """
        request = requests.get(f"https://api.bitbucket.org/2.0/repositories/{repository_path}/refs/tags?sort=-name&pagelen=100")
        request.raise_for_status()
        tags_objects = request.json()['values']
        request.close()

        version_pattern = re.compile(r'^(\d+)\.(\d+)\.(\d+)$')
        tags = [tag['name'] for tag in tags_objects if version_pattern.fullmatch(tag['name'])]

        if not tags:
            return None

        def parse_version(version):
            return tuple(map(int, version.split('.')))

        return max(tags, key=parse_version)

    def get_repository_info(self, repository_path: str) -> dict:
        """
        Retrieves repository information
        :param repository_path: Repository path (ex: account/repo)
        :return: JSON response of repository information
        """
        request = requests.get(f"https://api.bitbucket.org/2.0/repositories/{repository_path}/")
        request.raise_for_status()
        request.close()
        return request.json()

    def is_version_exists(self, repository_path: str, version: str) -> bool:
        """
        Determine if version (git tag) exists
        :param repository_path: Repository path (ex: account/repo)
        :param version: Reference (i.e. tag or commit)
        :return: True if exists, False otherwise
        """
        request = requests.head(f"https://api.bitbucket.org/2.0/repositories/{repository_path}/refs/tags/{version}")
        request.raise_for_status()
        request.close()
        return request.status_code == requests.codes.ok

    def readme_exists(self, repository_path: str, version: str) -> bool:
        """
        Determine if README.md file exists
        :param repository_path: Repository path (ex: account/repo)
        :param version: Reference (i.e. tag or commit)
        :return: True if exists, False otherwise
        """
        request = requests.head(f"https://bitbucket.org/{repository_path}/raw/{version}/README.md")
        request.raise_for_status()
        request.close()
        return request.status_code == requests.codes.ok

    def get_yml_definition(self, repository_path: str, version: str) -> Optional[str]:
        """
        Retrieves Yaml definition from the README.md file
        :param repository_path: Repository path (ex: account/repo)
        :param version: Reference (i.e. tag or commit)
        :return: String Yaml definition
        """
        request = requests.get(f"https://bitbucket.org/{repository_path}/raw/{version}/README.md")
        request.encoding = "utf-8"
        request.close()

        search_group = re.search(r"## YAML Definition(?:(?:.*?\n)*?)```yaml((?:.*?\n)*?)```$", request.text, re.MULTILINE)
        if search_group is None:
            return None
        yml_definition = (search_group.group(1)).lstrip()

        return yml_definition

    def get_pipe_metadata(self, repository_path: str, version: str) -> dict:
        """
        Retrieves pipe metadata
        :param repository_path: Repository path (ex: account/repo)
        :param version: Reference (i.e. tag or commit)
        :return: YAML object
        """
        request = requests.get(f"https://bitbucket.org/{repository_path}/raw/{version}/pipe.yml")
        request.raise_for_status()
        request.close()
        return yaml.safe_load(request.text)
