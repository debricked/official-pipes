'''
Docker
'''
import requests


class DockerHubAPIService:

    @staticmethod
    def get_size(docker_image_path: str) -> int:
        array = docker_image_path.split(':')
        tag = "latest"
        docker_image = array[0]
        if len(array) == 2:
            tag = array[1]
        request = requests.get(f"https://hub.docker.com/v2/repositories/{docker_image}/tags/{tag}")
        request.raise_for_status()
        request.close()
        size = request.json()['full_size']
        return int(size)

    @staticmethod
    def is_docker_image_exist(docker_image_path: str) -> int:
        docker_image_name, docker_image_version = docker_image_path.split(':')
        request = requests.head(f"https://hub.docker.com/v2/repositories/{docker_image_name}/tags/{docker_image_version}/")
        request.raise_for_status()
        request.close()
